@extends('admin/template_login')
@section('content')
    <body class="hold-transition register-page">
        <div class="register-box">
        <div class="register-logo">
            <a href="../../index2.html"><b>Admin</b>LTE</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Register a new membership</p>
            <form action="../../index.html" method="post" id="form_register">
            <div class="form-group has-feedback">
                <input type="text" name="full_name" id="full_name" class="form-control" required placeholder="Full name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="Email" name="email" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" />
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="text" name="user_name" id="user_name" class="form-control" required placeholder="Username">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" id="password" class="form-control" required placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="re_password" id="re_password" class="form-control" required placeholder="Retype password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            @if($errors->has('full_name'))
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{$errors->first('full_name')}}
                  </div>
              @endif
              @if($errors->has('email'))
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{$errors->first('email')}}
                  </div>
              @endif
              @if($errors->has('user_name'))
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{$errors->first('user_name')}}
                  </div>
              @endif
              @if($errors->has('password'))
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{$errors->first('password')}}
                  </div>
              @endif
            <div class="row">
                <div class="col-xs-8">
                </div>
                <!-- /.col -->
                {{ csrf_field() }}
                <div class="col-xs-4">
                <button class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>
            </form>

            <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
                Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
                Google+</a>
            </div>

            <a href="login.html" class="text-center">I already have a membership</a>
        </div>
        <!-- /.form-box -->
        </div>
        <!-- /.register-box -->
    </body>

<script>
    $(document).ready(function(){
        $('#form_register').validate({
            rule:{
                full_name: {
                    required: true,
                    minlength: 3,
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 6,
                },
                re_password: {
                    equalTo: "#password",
                },
            }
        });    
    });
    
</script>
@endsection