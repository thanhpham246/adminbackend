@extends('admin/template_login')
@section('content')
    <body class="hold-transition login-page">
      <div class="login-box">
        <div class="login-logo">
          <b>Admin</b>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
          <p class="login-box-msg">Sign in to start your session</p>

          <form action="/admin/checkLogin" method="post" id="form-login">
            <div class="form-group has-feedback">
              <input type="text" class="form-control" placeholder="Username" name="user_name" required />
            </div>
            <div class="form-group has-feedback">
              <input type="password" class="form-control" placeholder="Password" name="password" required />
            </div>
            @if($errors->has('errorlogin'))
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{$errors->first('errorlogin')}}
                  </div>
              @endif
              @if($errors->has('user_name'))
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{$errors->first('user_name')}}
                  </div>
              @endif
              @if($errors->has('password'))
                  <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      {{$errors->first('password')}}
                  </div>
              @endif
            <div class="row">
              <div class="col-xs-8">
                <div class="checkbox icheck">
                  <label>
                    <input name="remember" type="checkbox"> Remember Me
                  </label>
                </div>
              </div>
              <!-- /.col -->
              {{ csrf_field() }}
              <div class="col-xs-4">
                <button class="btn btn-primary btn-block btn-flat">Sign In</button>
              </div>
              <!-- /.col -->
            </div>
          </form>

          <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
              Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
              Google+</a>
          </div>
          <!-- /.social-auth-links -->

          <a href="#">I forgot my password</a><br>
          <a href="/admin/register" class="text-center">Register a new membership</a>

        </div>
        <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
    </body>
<script>
    $('#form-login').validate({
      rule:{
          user_name: {
              required: true,
              minlength: 5,
          },
          password: {
              required: true,
              minlength: 6,
          }
      }
    })
</script>
@endsection

