<?php

use Illuminate\Database\Seeder;

class tbl_category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_category')->insert([
            'category_name' => str_random(10),
            'category_slug' => str_random(10),
        ]);
    }
}
