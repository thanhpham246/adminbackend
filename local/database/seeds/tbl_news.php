<?php

use Illuminate\Database\Seeder;

class tbl_news extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_news')->insert([
            'news_name' => str_random(10),
            'news_slug' => str_random(10),
            'news_author' => str_random(10),
            'news_content' => str_random(100),
            'cat_id' => '1',
            
        ]);
    }
}
