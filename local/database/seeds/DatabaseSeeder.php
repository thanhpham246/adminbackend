<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(tbl_users::class);
        $this->call(tbl_category::class);
        $this->call(tbl_news::class);
    }
}
