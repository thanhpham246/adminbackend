<?php

use Illuminate\Database\Seeder;

class tbl_users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_users')->insert([
            'user_name' => 'admin',
            'full_name' => 'pham kim thanh',
            'group_users' => 1,
            'user_level' => 1,
            'email' => 'thanh.kjz@gmail.com',
            'password' => 'admin',
        ]);
    }
}
