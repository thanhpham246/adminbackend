<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_catalog', function (Blueprint $table) {
            $table->increments('cat_id');

            $table->string('cat_name');

            $table->string('cat_slug');

            $table->integer('parent_id');

            $table->string('cat_pic');

            $table->string('cat_pic_thumb');

            $table->integer('sort_order');

            $table->string('created_by');

            $table->string('updated_by');

            $table->integer('language');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_catalog');
    }
}
