<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_transaction', function (Blueprint $table) {
            $table->increments('transaction_id');

            $table->integer('user_id');

            $table->string('fullname');

            $table->string('email');

            $table->string('phone');

            $table->integer('amount');

            $table->string('payment'); //ten cong thanh toan

            $table->string('payment_info');

            $table->string('note');

            $table->integer('security'); // ma bao mat giao dich

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_transaction');
    }
}
