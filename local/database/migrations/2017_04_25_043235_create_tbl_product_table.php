<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product', function (Blueprint $table) {
            $table->increments('pro_id');

            $table->string('pro_name');

            $table->string('pro_slug');

            $table->integer('cate_id');

            $table->integer('price');

            $table->integer('discount');

            $table->string('pro_des');

            $table->string('pro_content');

            $table->string('pro_image');

            $table->string('pro_image_thumb');

            $table->integer('images_list_id');

            $table->string('created_by');

            $table->string('updated_by');

            $table->string('keyword_seo');

            $table->string('description_seo');

            $table->integer('language');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product');
    }
}
