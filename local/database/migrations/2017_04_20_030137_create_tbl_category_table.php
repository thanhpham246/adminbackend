<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_category', function (Blueprint $table) {
            $table->increments('cate_id');

            $table->string('cate_name');
 
            $table->string('cate_slug');

            $table->string('cate_pic');

            $table->string('cate_pic_thumb');

            $table->integer('sort_order');

            $table->integer('parent_id');

            $table->integer('language');

            $table->string('created_by');

            $table->string('updated_by');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_category');
    }
}
