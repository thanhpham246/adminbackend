<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_news', function (Blueprint $table) {
            $table->increments('news_id');

            $table->string('news_name');
            
            $table->string('news_slug');
            
            $table->string('news_img');

            $table->string('news_img_thumb');
            
            $table->string('news_author');

            $table->string('description');
            
            $table->text('news_content');
            
            $table->integer('cat_id');

            $table->integer('language');

            $table->integer('high_light');

            $table->integer('view_total');

            $table->string('id_link'); // bai viet lien quan

            $table->string('created_by');

            $table->string('updated_by');

            $table->string('keyword_seo');

            $table->string('description_seo');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_news');
    }
}
