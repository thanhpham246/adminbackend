<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_users', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('user_name');

            $table->string('password');
            
            $table->string('remember_token');

            $table->string('full_name');

            $table->string('address');

            $table->integer('phone');

            $table->string('email');

            $table->string('user_img');

            $table->string('user_img_thumb');

            $table->integer('country_id');

            $table->string('country_name');

            $table->integer('city_id');

            $table->string('city_name');
            
            $table->integer('group_users');

            $table->integer('user_level');

            $table->string('created_by');

            $table->string('updated_by');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_users');
    }
}
