<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('','HomeController@getIndex');
   

Route::group(array('prefix'=>'admin'),function(){
    Route::get('login','admin\LoginController@getLoginView');
    Route::get('register','admin\LoginController@getRegisterView');
    Route::post('checkLogin','admin\LoginController@getCheckLogin');        
    Route::get('logout','admin\LoginController@logOut');
    
    Route::group(array('middleware'=>'check-login'),function(){
        Route::get('/','admin\AdminController@index'); 
        Route::group(array('prefix'=>'users'),function(){
            Route::resource('index','admin\UserController');
            Route::resource('/','admin\UserController');
        });
        // Route::group(array('prefix'=>'admin-manager'),function(){
        //     Route::resource('index','admin\AdminController');
        //     Route::resource('/','admin\AdminController');
        // });
        
    });
});


Auth::routes();

Route::get('/home', 'HomeController@index');
