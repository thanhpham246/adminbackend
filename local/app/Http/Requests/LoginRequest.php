<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' =>'required:min:5',
    		'password' => 'required|min:6'
        ];
    }

    /**
     * Return the messages for request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_name.required' => 'Username là trường bắt buộc',
			'user_name.min' => 'Username phải chứa ít nhất 8 ký tự',
    		'password.required' => 'Password là trường bắt buộc',
    		'password.min' => 'Password phải chứa ít nhất 6 ký tự',
        ];
    }
}
