<?php

namespace App\Http\Middleware;

use Closure;
use App\admin\User;

class AdminLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('logined')) 
            return $next($request);
        else 
            return redirect('admin/login');
    }
}
