<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use App\admin\User;
use Illuminate\Support\MessageBag;

class LoginController extends Controller
{
    public function getLoginView() {
		if(Session::has("logined"))
			return redirect('/admin');
		else
        	return view('admin/login_view');
    }

    public function getCheckLogin(Request $request) {
        $rules = array(
			'user_name' =>'required:min:5',
    		'password' => 'required|min:6'
		);
		$messages = array(
			'user_name.required' => 'Username là trường bắt buộc',
			'user_name.min' => 'Username phải chứa ít nhất 8 ký tự',
    		'password.required' => 'Password là trường bắt buộc',
    		'password.min' => 'Password phải chứa ít nhất 6 ký tự',
		);
    	$validator = Validator::make($request->all(),$rules,$messages);

    	if ($validator->fails()) {
    		return redirect()->back()->withErrors($validator)->withInput();
    	} else {
			$user_name = $request->input('user_name');
			$password = $request->input('password');
			$arr = [
				'user_name' => $user_name,
				'password' => $password,
			];	
			
			if (User::where('user_name',$user_name)->where('password',$password)->count() == 1){
				$request->session()->put('logined', 'true');
				return redirect()->intended('/admin');	
			} else {
				$errors = new MessageBag(['errorlogin' => 'Tài khoản hoặc mật khẩu không đúng']);
    			return redirect()->back()->withInput()->withErrors($errors);	
			}
    	}
    }

	public function logOut(){
		if (Session::has('logined'))
			Session::flush();
			return redirect('/admin/login');
	}
}
